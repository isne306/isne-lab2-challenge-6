﻿#include<iostream>
#ifndef Graph
#define Graph
using namespace std;

class Node {
public:
	Node() { next = NULL; }
	Node(int ds, char el ,Node *n = 0) {
		distance = ds;
		element = el;
		next = n;
	}
	char element;
	int distance;
	Node *next;
};

class Edge {
public:
	Edge() { next = NULL; }
	Edge(char el) { ds = el; node = 0; next = 0; }
	char ds;
	Node *node;
	Edge *next;
};

class Graphs {
public:
	Graphs() { root = NULL; }
	void adjacencylist(int **store, int weight);
	void weights(int number);
	bool multiCheck();
	bool pseudoCheck();
	bool diCheck();
	bool weightCheck();
	bool completeCheck();
	Edge *root;
};

void Graphs::adjacencylist(int **store , int weight) {
	Edge *p_edge =root;
	Node *p_node=0;
	for (int i = 0; i < weight; i++) {
		if (i == 0) {
			root = new Edge(i+65);
			p_edge = root;
			p_node = p_edge->node;
		}
		else {
			p_edge->next = new Edge(i+65);
			p_edge = p_edge->next;
			p_node = p_edge->node;
		}
		for (int j = 0; j < weight; j++) {
			if (p_node == NULL && store[i][j] != -1) {
				p_edge->node = new Node(store[i][j], j+65);
				p_node = p_edge->node;
			}
			else if (store[i][j] != -1) {
				p_node->next = new Node(store[i][j], j+65);
				p_node = p_node->next;
			}
		}
	}
};

void Graphs::weights(int number) {
	Edge *p_edge = root ;
	Node *p_node ;
	for (int i = 0; i < number; i++){	
		char value = i + 65;
		cout << value << " -> ";
		p_node = p_edge->node;
		while (p_node != 0) {
			char value2 = p_node->element;
			cout << value2 << p_node->distance ;
			p_node = p_node->next;
			if (p_node != 0) { cout << " -> "; }
		}
		cout << endl;
		if(i != number - 1){
			cout << "|" << endl;
		}
		p_edge = p_edge->next;
	}
}

bool Graphs::multiCheck() {
	Edge *p_edge = root;
	Node *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			Edge *p_edge_2 = p_edge->next;
			while (p_edge_2 != NULL) {
				if (p_edge_2->ds == p_node->element) {
					Node *p_node2 = p_edge_2->node;
					while (p_node2 != NULL) {
						if (p_node2->element == p_edge->ds) {
							return true;
						}
						p_node2 = p_node2->next;
					}
				}
				p_edge_2 = p_edge_2->next;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return false;
}

bool Graphs::pseudoCheck() {
	Edge *p_edge = root;
	Node *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			if (p_node->element == p_edge->ds) {
				return true;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return false;
}

bool Graphs::diCheck() {
	Edge *p_edge = root;
	Node *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			Edge *p_edge_2 = p_edge->next;
			while (p_edge_2 != NULL) {
				if (p_edge_2->ds == p_node->element) {
					Node *p_node2 = p_edge_2->node;
					while (p_node2 != NULL) {
						if (p_node2->element == p_edge->ds) {
							if (p_node->distance == p_node2->distance) return false;
						}
						p_node2 = p_node2->next;
					}
				}
				p_edge_2 = p_edge_2->next;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return true;
}

bool Graphs::weightCheck() {
	Edge *p_edge = root;
	Node *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			if (p_node->distance != 0) {
				return true;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return false;
}

bool Graphs::completeCheck() {
	int count_node = 0;
	int sum_node=0;
	int count_edge=0;
	Edge *p_edge = root;
	Node *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		count_node = 0;
		while (p_node != NULL) {
			if (p_node->element != p_edge->ds)
				count_node++;
			p_node = p_node->next;
		}
		if (p_edge == root) {
			sum_node = count_node;
		}
		else {
			if (sum_node != count_node)return false;
		}
		count_edge++;
		p_edge = p_edge->next;
	}
	if (sum_node == count_edge - 1) return true;
	else return false;
}
#endif